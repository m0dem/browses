#!/usr/bin/python

from bs4 import BeautifulSoup

import curses
import sys
import urllib2


class TitleBar:
	def __init__(self, title):
		# pad spaces around the title
		self.title = title.rjust(len(title) + 1)
		self.title = self.title.ljust(WIDTH)

	def render(self, stdscr):
		for x in range(0, WIDTH):
			stdscr.addch(0, x, self.title[x - 1], curses.color_pair(1))


class Page:
	def __init__(self, webpage):
		self.webpage = webpage
		# change the height to be dynamic to webpage content
		self.width = WIDTH - 2
		self.height = int(len(webpage.content) / self.width) + 1
		#self.height, self.width = HEIGHT - 2, WIDTH - 2
		self.pad = curses.newpad(self.height, self.width)

		self.content = [self.webpage.content[i:i + self.width] for i in range(0, len(self.webpage.content), self.width)]

	def render(self, stdscr):
		for i, line in enumerate(self.content):
			for j, ch in enumerate(line):
				try:
					self.pad.addch(i, j, str(ch))

				except curses.error:
					pass

		self.pad.refresh(0, 0, 1, 1, HEIGHT - 1, WIDTH - 1)


class Webpage:
	def __init__(self, url):
		self.url = url
		self.raw = urllib2.urlopen(url).read()
		self.soup = BeautifulSoup(self.raw)
		# gets EVERYTHING (including title)
		self.content = self.soup.get_text()
		#self.content = [p.get_text() for p in self.soup.find_all("p")]


def init(stdscr):
	stdscr.clear()
	global HEIGHT, WIDTH
	HEIGHT, WIDTH = stdscr.getmaxyx()

	# TitleBar color
	curses.init_pair(1, curses.COLOR_WHITE, curses.COLOR_BLUE)
	# Link color
	curses.init_pair(2, curses.COLOR_RED, curses.COLOR_WHITE)

def main(stdscr):
	init(stdscr)
	webpage = Webpage(sys.argv[1])
	
	titleBar = TitleBar("My Test Title Bar")
	titleBar.render(stdscr)
	stdscr.refresh()
	page = Page(webpage)
	page.render(stdscr)
	
	# hook the program
	stdscr.getch()

curses.wrapper(main)
